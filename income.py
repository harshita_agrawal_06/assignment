import os


class TaxCalculator:
    def __init__(self):
        self.incomes = {}

    def add_user(self, name, income):
        user_name = name
        income_of_user = income
        self.incomes[user_name] = income_of_user

    def calculate_tax(self, name):
        base_income = self.incomes[name]
        if base_income <= 160000:
            tax = 0

        elif (base_income > 160000) and (base_income <= 500000):
            tax = (base_income - 160000) * 0.1

        elif (base_income > 500000) and (base_income <= 800000):
            tax = (base_income - 500000) * 0.2 + 34000

        else:
            tax = (base_income - 800000) * 0.3 + 94000

        return tax

    def readFromFile(self, filename):
        if not os.path.exists(filename):
            raise Exception("Bad File")
        infile = open(filename, "r")
        line = infile.readline()
        return line