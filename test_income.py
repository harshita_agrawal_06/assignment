import pytest
from unittest.mock import MagicMock

from _pytest.python_api import raises

from income import TaxCalculator


class TestIncome:
    @classmethod
    def setup_class(cls):
        print("Setup Test class")

    @classmethod
    def teardown_class(cls):
        print("Teardown Test Class")

    @pytest.fixture(autouse=True)
    def setup(self):
        print("\n Setup function")
        yield
        print("\n Teardown function")

    @pytest.fixture()
    def income(self):
        income = TaxCalculator()
        return income

    @pytest.fixture()
    def mock_open(self, monkeypatch):
        mock_file = MagicMock()
        mock_file.readline = MagicMock(return_value="74000")
        mock_open = MagicMock(return_value=mock_file)
        monkeypatch.setattr("builtins.open", mock_open)
        return mock_open

    # instantiated the class
    # def test_CanInstantiateIncome(self):
    #     inc = TaxCalculator()
    #
    #
    # def test_addUser(self, income):
    #     income.add_user("Sam", 500000)

    def test_calTax(self, income):
        income.add_user("Sam", 700000)
        assert income.calculate_tax("Sam") == 74000

    # def test_calCallReadFromFile(self, income):
    #     income.readFromFile("input_file")

    def test_returnsCorrectValue(self, income, mock_open, monkeypatch):
        mock_exists = MagicMock(return_value=True)
        monkeypatch.setattr("os.path.exists", mock_exists)
        result = income.readFromFile("input_file")
        mock_open.assert_called_once_with("input_file", "r")
        assert result == "74000"

    def test_throwsExceptionWithBadFile(self, income, mock_open, monkeypatch):
        mock_exists = MagicMock(return_value=False)
        monkeypatch.setattr("os.path.exists", mock_exists)
        with raises(Exception):
            result = income.readFromFile("input_file")